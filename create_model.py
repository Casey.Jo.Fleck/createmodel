import pandas as pd
import json
import requests
from bs4 import BeautifulSoup
import json


def csv_to_jsonl(csv_file_path, jsonl_file_path, r_username, r_accesskey, entity_id, entity_hash):
    df = pd.read_csv(csv_file_path)
    data = []
    for i in range(len(df)):
        # Make a call to the function
        json_response = extract_data(df['SERVICE_REQUEST_ID'][i], r_username, r_accesskey, entity_id, entity_hash)

        # Skip if json_response is None
        if json_response is None:
            continue

        # Load the JSON response
        loaded_data = json.loads(json_response)

        # Extract the 'combinedQA' field and 'taskOid' field
        data.append({"prompt": loaded_data.get('combinedQA'), "completion": loaded_data.get('taskOid')})

    with open(jsonl_file_path, 'w') as outfile:
        for entry in data:
            json.dump(entry, outfile)
            outfile.write('\n')



def get_entity_hash(entity_id):
    url = f"https://api.dev.angi.cloud/sm/debug/entity/{entity_id}"
    response = requests.get(url)

    if response.status_code == 200:
        soup = BeautifulSoup(response.text, "html.parser")
        entity_hash = soup.find("td", text="Secure Entity Hash").find_next_sibling("td").text
        return entity_hash
    else:
        return None


def extract_data(srOid, r_username, r_accesskey, entity_id, entity_hash):
    response = requests.get(
        f"http://potential-sr.dev.angi.cloud/resource/interview/{srOid}?r_username={r_username}&r_accesskey={r_accesskey}&entityId={entity_id}&entityHash={entity_hash}")

    if response.status_code == 200:
        data = response.json()
        combined_qa = ''
        task_oid = None

        for item in data:
            task_oid = item.get("taskOid")  # Assigns taskOid once as it is the same in all the json objects
            combined_qa += f'{item["questionText"]} {item["answerText"]} '

        json_data = {
            "taskOid": task_oid,
            "combinedQA": combined_qa.strip()  # removes any trailing space
        }

        return json.dumps(json_data)

    else:
        return None