import create_model
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()


def main():
    r_username = os.getenv("R_USERNAME")
    r_accesskey = os.getenv("R_ACCESSKEY")
    entity_id = os.getenv("ENTITY_ID")
    entity_hash = create_model.get_entity_hash(entity_id)
    if entity_hash:
        create_model.csv_to_jsonl('TASKOID_SERVICE_REQUEST_ID.csv', 'output.jsonl', r_username, r_accesskey, entity_id,
                                  entity_hash)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
